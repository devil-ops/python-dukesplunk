#!/usr/bin/env python3
import sys
from pydukesplunk.helpers import get_credentials
from pydukesplunk.splunk import api
import argparse
import socket
import re


def parse_args():
    """Parse arguments
    """
    parser = argparse.ArgumentParser(
        description='Search firewall logs')
    parser.add_argument('-c', '--count', type=int, default=100,
                        help="Number of results to return (0 for unlimited)")
    parser.add_argument('-e', '--earliest', help='Earliest time to query',
                        default='1 hour ago')
    parser.add_argument('-l', '--latest', help='Latest time to query',
                        default='now')
    parser.add_argument('-d', '--destination',
                        help='Filter based on the destination')
    parser.add_argument('-s', '--source',
                        help='Filter based on the source')
    parser.add_argument('-r', '--vrf',
                        help='Filter based on the VRF')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='Verbose output')
    parser.add_argument('-p', '--port',
                        help=(
                            'Filter based on the port.  This can be either '
                            'the number, or a resolvable service name'
                        ))
    parser.add_argument(
        '-n', '--no-resolve', action='store_true',
        help='Show ip and port number instead of resolved names')
    return parser.parse_args()


def is_int(possible_int):
    test = re.compile('\d+')
    result = test.match(possible_int)
    if result:
        return True
    else:
        return False


def is_ip(possible_ip):
    test = re.compile('\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b')
    result = test.match(possible_ip)
    if result:
        return True
    else:
        return False


def lookup_ip(ip):
    try:
        return socket.gethostbyaddr(ip)[0]
    except Exception as e:
        return ip


def lookup_port(port):
    try:
        return socket.getservbyport(int(port))
    except Exception as e:
        return port


def main():
    """Meat of the program, mmmmmm yum
    """

    args = parse_args()
    credentials = get_credentials()

    splunk = api(username=credentials['username'],
                 password=credentials['password'],
                 server=credentials['server'])

    # Base filter
    search_params = {
        'earliest_time': args.earliest,
        'latest_time': args.latest
    }

    # Add a filter based on destination
    if args.destination:
        if not is_ip(args.destination):
            to_address = socket.gethostbyname(args.destination)
        search_params['to_ipaddress'] = to_address

    # Add a filter based on source
    if args.source:
        if not is_ip(args.source):
            from_address = socket.gethostbyname(args.source)
        search_params['from_ipaddress'] = from_address

    # Add a filter based on the vrf
    if args.vrf:
        search_params['vrf'] = args.vrf

    # Add a port filter
    if args.port:
        if is_int(args.port):
            port = args.port
        else:
            port = socket.getservbyname(args.port)
        search_params['to_port'] = port

    results = splunk.search_firewall(**search_params)

    for item in results:
        dict_item = dict(item)
        dict_item['from_dns'] = lookup_ip(dict_item['from_ipaddress'])
        dict_item['to_dns'] = lookup_ip(dict_item['to_ipaddress'])
        dict_item['to_service'] = lookup_port(dict_item['to_port'])
        if args.no_resolve:
            print(
                "{_time} {vrf:14s}"
                " {from_ipaddress:20s} -> "
                "{to_ipaddress}:{to_port}".format(**dict_item))
        else:
            print(
                "{_time} {vrf:14s}"
                " {from_dns:35s} -> "
                "{to_dns}:{to_service}".format(**dict_item))

        # If requested, print the actual log
        if args.verbose:
            print(dict_item['_raw'])


if __name__ == "__main__":
    sys.exit(main())
