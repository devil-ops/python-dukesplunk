#!/usr/bin/env python3
import sys
from pydukesplunk.helpers import get_credentials
from pydukesplunk.splunk import api
import argparse
import json


def parse_args():
    """Parse arguments
    """
    parser = argparse.ArgumentParser(
        description='Do a splunk search from the command line')
    parser.add_argument('query', type=str, help="Splunk query to use")
    parser.add_argument('-f', '--format', choices=['raw', 'json'],
                        default='raw', help='Format to output in')
    parser.add_argument('-c', '--count', type=int, default=100,
                        help="Number of results to return (0 for unlimited)")
    parser.add_argument('-e', '--earliest', help='Earliest time to query',
                        default='1 hour ago')
    parser.add_argument('-l', '--latest', help='Latest time to query',
                        default='now')
    parser.add_argument('-s', '--summary', action='store_true',
                        help='Show summary of interesting things after logs')
    return parser.parse_args()


def main():
    """Meat of the program, mmmmmm yum
    """

    args = parse_args()
    credentials = get_credentials()

    splunk = api(username=credentials['username'],
                 password=credentials['password'],
                 server=credentials['server'])

    results = splunk.search(args.query, return_count=args.count,
                            earliest_time=args.earliest)
    summary = {
        'indexes': {}
    }
    if args.format == 'raw':
        for item in results:
            dict_item = dict(item)
            print(dict_item['_raw'])
            if dict_item['index'] not in summary['indexes'].keys():
                summary['indexes'][dict_item['index']] = 1
            else:
                summary['indexes'][dict_item['index']] += 1
    else:
        sys.stderr.write("Go get some coffee while we convert this to json\n")
        output = []
        for item in results:
            output.append(item)
        print(json.dumps(output))

    if args.summary:
        print(summary)


if __name__ == "__main__":
    sys.exit(main())
