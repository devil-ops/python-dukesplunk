import os
from setuptools import setup, find_packages


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
    name="python-dukesplunk",
    version="0.0.3",
    author="Drew Stinnett",
    author_email="drew.stinnett@duke.edu",
    description=("Interact with Duke instance of Splunk with Python"),
    license="BSD",
    keywords="splunk python cli",
    packages=find_packages(),
    scripts=[
        'examples/splunk-search.py',
        'examples/splunk-search-firewall.py'
    ],
    long_description=read('README.md'),
)
