import splunklib.results as results
import splunklib.client as client
from time import sleep
import progressbar
import dateparser


class api(object):
    """Splunk object
    """
    pass

    def __init__(self, *args, **kwargs):
        username = kwargs.get('username')
        password = kwargs.get('password')
        server = kwargs.get('server')

        self.service = client.connect(username=username, password=password,
                                      host=server)

    def search(self, *args, **kwargs):
        """Do a splunk search

        Parameters
        ----------
        query : str
          Splunk string to query
        earliest_time: str
          Earliest time to query, must be parseable with dateparser
        latest_time: str
          Latest time to query, must be parseable with dateparser
        return_count : int
          Number of results to return.  Defaults to 100
        """
        query = args[0]
        return_count = kwargs.get('return_count', 100)
        earliest_time = kwargs.get('earliest_time', '1 hour ago')
        latest_time = kwargs.get('latest_time', 'now')

        earliest_time_obj = dateparser.parse(earliest_time)
        latest_time_obj = dateparser.parse(latest_time)

        kwargs_normalsearch = {
            "exec_mode": "normal",
            'enable_lookups': True,
            'earliest_time': earliest_time_obj.isoformat(),
            'latest_time': latest_time_obj.isoformat(),
            "count": return_count
        }
        if query[0] != '|':
            real_query = "{}".format(query)
        else:
            real_query = query

        job = self.service.jobs.create("search %s" % real_query,
                                       **kwargs_normalsearch)
        widgets = ['Searching Splunk ', progressbar.Percentage(),
                   progressbar.Bar(), ' ',
                   progressbar.ETA()]
        with progressbar.ProgressBar(max_value=100, widgets=widgets,
                                     redirect_stdout=True) as p:
            while True:
                while not job.is_ready():
                    pass
                stats = {
                    "isDone": job["isDone"],
                    "doneProgress": float(job["doneProgress"])*100,
                    "scanCount": int(job["scanCount"]),
                    "eventCount": int(job["eventCount"]),
                    "resultCount": int(job["resultCount"])
                }

                if stats["isDone"] == "1":
                    break
                sleep(1)

                if stats['doneProgress'] < 100:
                    try:
                        p.update(float(stats['doneProgress'])*100)
                    except Exception as e:
                        continue

            return_items = results.ResultsReader(job.results(
                **kwargs_normalsearch))
            job.cancel()

        return return_items

    def search_firewall(self, *args, **kwargs):
        """Search for firewall specific logs
        Parameters
        ----------
        from_ipaddress: str
          Filter based on the source address
        to_ipaddress: str
          Filter based on the destination address
        to_port: str
          Filter based on the destination port
        from_ipaddress: str
          Filter based on the source address
        vrf: str
          Filter based on VRF
        earliest_time: str
          Earliest time to query, must be parseable with dateparser
        latest_time: str
          Latest time to query, must be parseable with dateparser
        """

        to_ipaddress = kwargs.get('to_ipaddress', '*')
        to_port = kwargs.get('to_port', '*')
        from_ipaddress = kwargs.get('from_ipaddress', '*')
        vrf = kwargs.get('vrf', '*')
        earliest_time = kwargs.get('earliest_time', '1 hour ago')
        latest_time = kwargs.get('latest_time', 'now')

        query_list = []

        # Filter as much as we can up front, this will clean up later
        query_list.append('index=*network_firewall* deny %s %s %s' % (
            to_ipaddress, from_ipaddress, vrf))

        query_list.append(
            "| rex \"(?<vrf>\S+) : .*?(?:outside:|inside:|from )"
            "(?<from_ipaddress>\S+)/(?<from_port>\d+) (dst \S+:|to )"
            "(?<to_ipaddress>\S+)/(?<to_port>\d+)\""
        )

        # Now get an exact match
        query_list.append(
            "| search to_ipaddress=%s from_ipaddress=%s vrf=%s to_port=%s" % (
                to_ipaddress, from_ipaddress, vrf, to_port))

        query = "".join(query_list)
        print(query)
        return_value = self.search(
            query,
            earliest_time=earliest_time,
            latest_time=latest_time,
            return_count=0
        )
        return return_value
