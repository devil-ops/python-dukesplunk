import os
from devilparser import rcfile


def get_credentials(*args, **kwargs):
    """Return login credentials and information for connecting to splunk
    """

    config_file = kwargs.get(
        'config_file', os.path.expanduser('~/.splunk.yaml'))

    secret_data = rcfile.parse(config_file)
    return {
        'username': secret_data['username'],
        'password': secret_data['password'],
        'server': secret_data['server']
    }
