# Setup

Configure your ~/.splunk.yaml file with something like:

```
---
main:
  server: splunk.oit.duke.edu
  username: $ vault read -field username secret/puppet/splunk/admin/splunk-oit.oit.duke.edu
  password: $ vault read -field password secret/puppet/splunk/admin/splunk-oit.oit.duke.edu
```

# splunk-search.py
## Usage:
```
usage: splunk-search.py [-h] [-f {raw,json}] [-c COUNT] [-e EARLIEST]
                        [-l LATEST] [-s]
                        query

Do a splunk search from the command line

positional arguments:
  query                 Splunk query to use

optional arguments:
  -h, --help            show this help message and exit
  -f {raw,json}, --format {raw,json}
                        Format to output in
  -c COUNT, --count COUNT
                        Number of results to return (0 for unlimited)
  -e EARLIEST, --earliest EARLIEST
                        Earliest time to query
  -l LATEST, --latest LATEST
                        Latest time to query
  -s, --summary         Show summary of interesting things after logs
```
## Notes

For some reason, starting the query with an inputlookup does not work in the api

Instead of:

```
| inputlookup cmdb.csv
```

Use:

```
sourcetype=something_doesnt_exist | inputlookup append=t cmdb.csv'
```

# splunk-search-firewall.py

Search the firewall logs specifically, and format as such

## Usage

```
usage: splunk-search-firewall.py [-h] [-c COUNT] [-e EARLIEST] [-l LATEST]
                                 [-d DESTINATION] [-s SOURCE] [-r VRF] [-v]
                                 [-p PORT] [-n]

Search firewall logs

optional arguments:
  -h, --help            show this help message and exit
  -c COUNT, --count COUNT
                        Number of results to return (0 for unlimited)
  -e EARLIEST, --earliest EARLIEST
                        Earliest time to query
  -l LATEST, --latest LATEST
                        Latest time to query
  -d DESTINATION, --destination DESTINATION
                        Filter based on the destination
  -s SOURCE, --source SOURCE
                        Filter based on the source
  -r VRF, --vrf VRF     Filter based on the VRF
  -v, --verbose         Verbose output
  -p PORT, --port PORT  Filter based on the port. This can be either the
                        number, or a resolvable service name
  -n, --no-resolve      Show ip and port number instead of resolved names
```
